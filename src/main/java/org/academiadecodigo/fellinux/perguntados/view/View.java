package org.academiadecodigo.fellinux.perguntados.view;

public interface View {
    void show();
}
